<?php

namespace Drupal\paldesk\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides the path admin overview form.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'paldesk_admin_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['paldesk.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('paldesk.settings');

    $form['widget_api_key'] = [
      '#type' => 'textfield',
      '#title' => t('Website Widget API key'),
      '#default_value' => $config->get('widget_api_key'),
      '#description' => t('
      To find API Key, click on <b>Administration</b> > <b>Website</b> <b>Widget</b> > <b>Install</b> > <b>Copy</b> <b>API</b> <b>Key</b> and <b>paste</b> it in the field above.

            ',
         [
           ':paldesk_url' =>
           'https://www.paldesk.com',
         ]),
      '#size' => 70,
      '#maxlength' => 70,
      '#required' => FALSE,
    ];

    $form['notification_api_key'] = [
      '#type' => 'textfield',
      '#title' => t('Easy Popup API key'),
      '#default_value' => $config->get('notification_api_key'),
      '#description' => t('
            To find API Key, click on <b>Administration</b> > <b>Easy</b> <b>Popup</b> > <b>Install</b> > <b>Copy</b> <b>API</b> <b>Key</b> and <b>paste</b> it in the field above.

            ',
         [
           ':paldesk_url' =>
           'https://www.paldesk.com',
         ]),
      '#size' => 70,
      '#maxlength' => 70,
      '#required' => FALSE,
    ];
    $form['feedback_api_key'] = [
      '#type' => 'textfield',
      '#title' => t('Feedback API key'),
      '#default_value' => $config->get('feedback_api_key'),
      '#description' => t('
            To find API Key, click on <b>Administration</b> > <b>Feedback</b> > <b>Install</b> > <b>Copy</b> <b>API</b> <b>Key</b> and <b>paste</b> it in the field above.

            ',
         [
           ':paldesk_url' =>
           'https://www.paldesk.com',
         ]),
      '#size' => 70,
      '#maxlength' => 70,
      '#required' => FALSE,
    ];

    $form['visibility'] = [
      '#type' => 'radios',
      '#title' => t('Show Paldesk widgets on specific pages'),
      '#options' => [
        PALDESK_BLACKLIST_MODE => t('Hide for the listed pages'),
        PALDESK_WHITELIST_MODE => t('Show for the listed pages'),
      ],
      '#default_value' => $config->get('visibility'),
    ];
    $form['pages'] = [
      '#type' => 'textarea',
      '#title' => '<span class="visually-hidden">' . t('Pages') . '</span>',
      '#default_value' => _paldesk_array_to_string($config->get('pages')),
      '#description' => t("Specify pages by using their paths. 
            Enter one path per line. The '*' character is a wildcard. 
            Example paths are %user for the current user's page 
            and %user-wildcard for every user page. %front is the front page.",
        [
          '%user' => '/user',
          '%user-wildcard' => '/user/*',
          '%front' =>
          '<front>',
        ]),
    ];
    $form['roles'] = [
      '#type' => 'checkboxes',
      '#title' => t('Hide Paldesk widgets for specific roles'),
      '#options' => array_map('\Drupal\Component\Utility\Html::escape', user_role_names()),
      '#default_value' => $config->get('roles'),
      '#description' => t('Hide Paldesk widgets only for the selected
            role(s). If none of the roles are selected, all roles will have the 
            widgets. Otherwise, any roles selected here will NOT have the widgets.'
      ),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();

    $this->config('paldesk.settings')
      ->set('widget_api_key', $values['widget_api_key'])
      ->set('notification_api_key', $values['notification_api_key'])
      ->set('feedback_api_key', $values['feedback_api_key'])
      ->set('visibility', $values['visibility'])
      ->set('pages', _paldesk_string_to_array($values['pages']))
      ->set('roles', array_filter($values['roles']))
      ->save();

    parent::submitForm($form, $form_state);
  }

}
