// paldesk configuration
var paldeskConfig = {
	apiKey: drupalSettings.paldesk.notification_api_key,
};


var script = document.createElement('script');
script.async = true;
script.src = "https://paldesk.io/api/nwidget-client?apiKey=" + paldeskConfig.apiKey;
//alert( "paldeskConfig.apiKey =" +  paldeskConfig.apiKey)
script.onload = function () {
	/*
	here you can add user_data to config object
	eg.
	beebeeate_config.user_data = {
		externalId: "your_id_for_user"
		username: "username",
		first_name: "user_first_name",
		last_name: "user_last_name",
	}
	*/
	var instance = BeeBeeateNotification.widget.new(beebeeate_config_notification);
}
document.body.appendChild(script);
